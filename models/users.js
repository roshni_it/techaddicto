const mongoose = require('mongoose');
const titlize = require('mongoose-title-case');
const validate = require('mongoose-validator');
const bcrypt = require('bcrypt');

// Name Validator
const nameValidator = [
    validate({
        validator: 'matches',
        arguments: /^(([a-zA-Z]{3,20})+[ ]+([a-zA-Z]{3,20})+)+$/,
        message: 'Must be atleast 3 Char, max 20 Char, No Special Chars,  Space between Name and Surname.'
    })
];

// username Validator
const usernameValidator = [
    validate({
        validator: 'isLength',
        arguments: [6, 25],
        message: 'Username should be between {ARGS[0]} and {ARGS[1]} characters'
    }),
    validate({
        validator: 'isAlphanumeric',
        message: 'Username must contain letters and numbers only'
    })
];

//Contact validator
const contactValidator = [
    validate({
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value'
    })
];

// Email Validator
var emailValidator = [
    validate({
        validator: 'matches',
        arguments: /^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/,
        message: 'Name must be at least 3 characters, max 40, no special characters or numbers, must have space in between name.'
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 40],
        message: 'Email should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

// Password Validator
var passwordValidator = [
    validate({
        validator: 'matches',
        arguments: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/,
        message: 'Password needs to have at least one lower case, one uppercase, one number, one special character, and must be at least 8 characters but no more than 35.'
    }),
  /*  validate({
        validator: 'isLength',
        arguments: [6, 35],
        message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
    })*/
];

// Create UserSchema
const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        validate: nameValidator
    },
    username: {
        type: String,
        required: true,
        unique: true,
        validate: usernameValidator
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: emailValidator
    },
    contact: {
        type: Number,
        validate: contactValidator
    },
    password: {
        type: String,
        required: true,
       /* validate: passwordValidator*/
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        required:true,
        default: false
    },
    temporarytoken: {
        type: String,
        required:true,
    },
    resettoken: {
        type: String,
        required:false,
    }
});

// Attach some mongoose hooks 
userSchema.plugin(titlize, {
  paths: ['name']
});


// Export Module

const User = module.exports = mongoose.model('User', userSchema);

// Write all Functions For CRUD(Create, Read, Update, Delete) Operations

// Get all Users (Read Operation)
module.exports.getUsers = function(callback){
    User.find(callback);
}

// Get User By Id
module.exports.getUserById = function(user_id, callback){
    User.findById(user_id, callback);
}

// Get User By username
module.exports.getUserByUsername = function(username, callback){
    const query = {username: username};
    User.findOne(query, callback);
}

// Add User (Create Operation)
module.exports.addUser = function(newUser, callback){
    // Simple method to add user
    /* User.create(newUser, callback); */

    //Add user with encrypted password
    bcrypt.genSalt(10, (err, salt) => {
        if(err){
            throw err;
        }else{
            bcrypt.hash(newUser.password, salt, (err, hash) => {
                if(err){
                    throw err;
                }else{
                    newUser.password = hash;
                    newUser.save(callback);
                }
            });
        }
    });
}

// Delete user (Delete Operation)
module.exports.deleteUser = function(username, callback){
    const query = {username: username};
    User.findOneAndRemove(query, callback);
}

// Compare Password for Authentication
module.exports.comparePassword = function(condidatePassword, hash, callback){
    bcrypt.compare(condidatePassword, hash, (err, IsMatch) =>{
        if(err) throw err;
        callback(null, IsMatch);
    })
}

// Update User  
 // Work In Progress