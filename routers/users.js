const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');
const bcrypt = require('bcrypt');

// call Database and model File
const config = require('../config/database');
const User = require('../models/users');

// Write all HTTP requests (get, post, put, delete)

// Get all users on url: http://localhost/userapi/users on "Get" request
router.get('/users', (req, res) => {
    User.getUsers(function(err, users){
        if(err){
            throw err;
        }else{
            res.json(users);
        }
    })
});


// Sendgrid And Nodemailer Configuration
const options = {
        service : 'SendGrid',
        auth: {
            api_user: 'roshni090',
            api_key: 'asdfghjkl12'
        }
    }
    const client = nodemailer.createTransport(sgTransport(options));

    // Register new user on url: http://localhost/userapi/users on "Post" request
router.post('/register', (req, res, next) => {
    const newUser = new User({
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        contact: req.body.contact,
        password: req.body.password,
        temporarytoken: jwt.sign({name: req.body.name, username: req.body.username, email: req.body.email, contact: req.body.contact, password: req.body.password }, config.secret, {expiresIn: 86400})
    });
    if(req.body.name == '' || req.body.name == null || req.body.username == '' || req.body.username == null || req.body.email == '' || req.body.email == null || req.body.contact == '' || req.body.contact == null || req.body.password == '' || req.body.password == null){
        res.json({success: false, msg: 'Mandatory Fields cannot be Blanks!'});
    }else{
        User.addUser(newUser, function(err, user){
            if(err) {
                if(err.errors != null){
                    if (err.errors.name) {
                        res.json({ success: false, msg: err.errors.name.message }); // Display error in validation (name)
                    } else if (err.errors.email) {
                        res.json({ success: false, msg: err.errors.email.message }); // Display error in validation (email)
                    } else if (err.errors.username) {
                        res.json({ success: false, msg: err.errors.username.message }); // Display error in validation (username)
                    } else if (err.errors.contact) {
                        res.json({ success: false, msg: err.errors.contact.message }); // Display error in validation (password)
                    } else if (err.errors.password) {
                        res.json({ success: false, msg: err.errors.password.message }); // Display error in validation (password)
                    } else {
                        res.json({ success: false, msg: err }); // Display any other errors with validation
                    } 
                }else if(err){
                    if(err.code == 11000){
                        if(err.errmsg[64] == 'u'){
                            res.json({success: false, msg: 'Username already exist'});
                        }else if(err.errmsg[64] == 'e'){
                            res.json({success: false, msg: 'Email already exist'});
                        }  
                    }else{
                        res.json({success: false, msg: err}); 
                    }
               }
            }else{

                var mailOptions = {    
                    from: 'roshniagrawal.it@gmail.com',
                    to: user.email,
                    subject: 'Account Activation Link',
                    text: 'Hello ' + user.name + ', Thank you for registering on Techaddicto. Please Click on the link below to activate your account. Click Here: http://localhost:3000/activate' + user.temporarytoken,

                    html: 'Hello<strong> ' + user.name + '</strong>,<br><br> Thank you for registering on <strong>Techaddicto</strong>. Please Click on the link below to activate your account.<br><br><br> Click Here: <a href="http://localhost:3000/activate/' + user.temporarytoken + '">http://localhost:3000/activate/' + user.temporarytoken + '</a>'
                }

                client.sendMail(mailOptions, function(err, response){
                    if (err){
                        console.log(err);
                    }
                    else {
                        console.log('Message sent: ' + response.message);
                    }
                    client.close();
                });
                res.json({success: true, msg: 'Successfully registered User! Please check your mail to activate account.'});
            }
        })
    }
});


// Get user account activate by clicking on link send in the user Email
router.put('/activate/:token', (req, res) => {
    User.findOne({temporarytoken: req.params.token}, function(err, user){
        if(err) throw err;
        var token = req.params.token; 
        //verify token
        jwt.verify(token, config.secret , function(err, decoded) {
            if(err){
                res.json({success: false, msg: 'Activation Link expired'});
            }else if(!user){
                res.json({success: false, msg: 'Activation Link expired'});
            }else{
                user.temporarytoken = false;
                user.active = true;
                user.save(function(err){
                    if(err){
                            console.log(err); 
                        }else{
                        var mailOptions = {    
                            from: 'Localhost stuff, staff@localhost.com',
                            to: user.email,
                            subject: 'Account Activate Successfully',
                            text: 'Hello ' + user.name + ', Your Account has been successfully activated!',
                            html: 'Hello<strong> ' + user.name + '</strong>,<br><br> Your Account has been successfully activated!'
                        };

                        client.sendMail(mailOptions, function(err, response){
                            if (err){
                                console.log(err);
                            }
                            else {
                                console.log('Message sent: ' + response.message);
                            }
                            client.close();
                        });
                            res.json({success: true, msg: 'Account Activated'});
                    }
                })
            }
        });
    })
});

// Get existing user on url: http://localhost/userapi/users/username on "Read" request
router.get('/users/:username', (req, res, next) => {
     const username = req.params.username;
     User.getUserByUsername(username, function(err, user){
        if(err){
            res.json({success: false, msg: "Failed to retrive User"});
        }else{
            res.json(user);
        }
    });
});


// Delete existing user on url: http://localhost/userapi/users/username on "Delete" request
router.delete('/users/:username', (req, res, next) => {
    const userName = req.params.username;
    User.deleteUser(userName, function(err, user){
        if(err){
            res.json({success: false, msg: "Failed to Delete User"});
        }else{
            res.json({success: true, msg: "User has been Deleted successfully"});
        }
    });
});

// Check if Username alresdy exist
router.post('/checkusername', function(req, res){
    const username = req.body.username;
    User.findOne({username: username}).select('username').exec(function(err, user){
        if(err) throw err;
        if(user){
            res.json({success: false, msg: 'Username already exist.'});
        }else{
            res.json({success: true, msg: 'Valid Username'});
        }
    });
});

// Check if User Emailid alresdy exist
router.post('/checkemail', function(req, res){
    const email = req.body.email;
    User.findOne({email: email}).select('email').exec(function(err, user){
        if(err) throw err;
        if(user){
            res.json({success: false, msg: 'Email id already exist.'});
        }else{
            res.json({success: true, msg: 'Valid EmailId'});
        }
    });
});

// Authenticate user on url: http://localhost/userapi/authenticate on "post" request
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    if(username == '' || username == null || password == '' || password == null ){
        res.json({success: false, msg: 'Username or Password cannot be blank!'});
    }else{
        User.getUserByUsername(username, function(err, user){
            if(err) throw err;
            if(!user){
                res.json({success: false, msg: 'User not Exist'});
            }else{
                User.comparePassword(password, user.password, (err, IsMatch) => {
                    if(err) throw err;
                    if(!user.active){
                        res.json({success: false, msg: 'Account is not yet activated', expired: true});
                    }
                    else if(IsMatch){
                        const token = jwt.sign(user, config.secret, {expiresIn: '15s'});
                        res.json({
                            success: true,
                            token: token,
                            user:{
                                Id: user._id,
                                name: user.name,
                                username: user.username,
                                email: user.email,
                                contact: user.contact
                            },
                            msg: 'Authenticate Successfully'
                        })
                    }else{
                      res.json({success: false, msg: 'Wrong Password'});  
                    }
                })
            }
        })
    }
});

// Authenticate user and Resend Account Activation Link on url: http://localhost/userapi/resend on "post" request
// Authenticate user
router.post('/resend', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    console.log(username);
    if(username == '' || username == null || password == '' || password == null ){
        res.json({success: false, msg: 'Username or Password cannot be blank!'});
    }else{
        User.getUserByUsername(username, function(err, user){
            if(err) throw err;
            if(!user){
                res.json({success: false, msg: 'User not Exist'});
            }else{
                User.comparePassword(password, user.password, (err, IsMatch) => {
                    if(err) throw err;
                    if(user.active){
                        res.json({success: false, msg: 'Account is already activated'});
                    }
                    else if(IsMatch){
                       // const token = jwt.sign(user, config.secret, {expiresIn: 86400});
                        res.json({success: true, user: user});
                    }else{
                      res.json({success: false, msg: 'Wrong Password'});  
                    }
                })
            }
        })
    }
});
// Resend Account Activation Link
router.put('/resend', (req, res) => {
    User.findOne({username: req.body.username}, function(err, user){
        if(err) throw err;
        user.temporarytoken = jwt.sign(user, config.secret, {expiresIn: 86400});
        user.save(function(err){
            if(err){
                console.log(err);
            }else{
                var mailOptions = {    
                    from: 'roshniagrawal.it@gmail.com',
                    to: user.email,
                    subject: 'Re: Account Activation Link',
                    text: 'Hello ' + user.name + ', Thank you for reactivation account link. Please Click on the link below to activate your account. Click Here: http://localhost:3000/activate' + user.temporarytoken,

                    html: 'Hello<strong> ' + user.name + '</strong>,<br><br> Thank you for reactivation account link . Please Click on the link below to activate your account.<br><br><br> Click Here: <a href="http://localhost:3000/activate/' + user.temporarytoken + '">http://localhost:3000/activate/' + user.temporarytoken + '</a>'
                }

                client.sendMail(mailOptions, function(err, response){
                    if (err){
                        console.log(err);
                    }
                    else {
                        console.log('Message sent: ' + response.message);
                    }
                    client.close();
                });
                res.json({success: true, msg: 'Activation link has been sent to '+ user.email});
            }
        })
    })
});

// Get username in registered mail id
router.get('/resetusername/:email', (req, res) => {
    const email = req.params.email;
    if(email == '' || email == null ){
        res.json({success: false, msg: 'Email was not Provided'});  
    }else{
        User.findOne({email: email}, function(err, user){
            if(err){
                res.json({success: false, msg: err});
            }else{
               if(!user){
                    res.json({success: false, msg: 'Email was not found'});
                }else{
                    var mailOptions = {    
                        from: 'roshniagrawal.it@gmail.com',
                        to: user.email,
                        subject: 'Forget Username Request',
                        text: 'Hello ' + user.name + ', You have request for the username. Username: '+ user.username,

                        html: 'Hello<strong> ' + user.name + '</strong>,<br><br> You have request for the username. <br><br><ul><li> <strong>Username: </strong>'+ user.username +'</li><li> <strong>Email:  </strong>'+ user.email +'</li></ul>'
                    }

                    client.sendMail(mailOptions, function(err, response){
                        if (err){
                            console.log(err);
                        }
                        else {
                            console.log('Message sent: ' + response.message);
                        }
                        client.close();
                    });
                    res.json({success: true, msg: 'Username has been sent to your registered Email.'});
                }
            }
        })
    }
    
});

// Reset Password for registerd Username
router.put('/resetpassword', (req, res) => {
    User.findOne({username: req.body.username}, function(err, user){
        if(err) throw err;
        if(!user){
            res.json({success: false, msg: 'Username was not found'});
        }else if(!user.active){
            res.json({success: false, msg: 'Cannot reset password, Account is not activated yet'});
        }else{
            user.resettoken = jwt.sign(user, config.secret, {expiresIn: 86400});
            user.save(function(err){
                if(err){
                    res.json({success: false, msg:err});
                }else{
                    var mailOptions = {    
                        from: 'roshniagrawal.it@gmail.com',
                        to: user.email,
                        subject: 'Re: Reset password request',
                        text: 'Hello ' + user.name + ',You have requested to reset password Link. Please Click on the link below to reset your Password. Click Here: http://localhost:3000/passwordreset/' + user.resettoken,

                        html: 'Hello<strong> ' + user.name + '</strong>,<br><br> You have requested to reset password Link. Please Click on the link below to reset your Password.<br><br><br> Click Here: <a href="http://localhost:3000/passwordreset/' + user.resettoken + '">http://localhost:3000/passwordreset/' + user.resettoken + '</a>'
                    }

                    client.sendMail(mailOptions, function(err, response){
                        if (err){
                            console.log(err);
                        }
                        else {
                            console.log('Message sent: ' + response.message);
                        }
                        client.close();
                    });
                    res.json({success: true, msg: 'Please Check your email for reset password link'});
                }
            })
        }
    })
});

// get user who wanna to change their password
router.get('/resetpassword/:token', (req, res) => {
    User.findOne({resettoken: req.params.token}, function(err, user){
        if(err) throw err;
        var token = req.params.token;
        jwt.verify(token, config.secret , function(err, decoded) {
            if(err){
                res.json({success: false, msg: 'Password Link expired'});
            }else{
                if(!user){
                    res.json({success: false, msg: 'Password reset link Expired'});
                }else{
                    res.json({success: true, user: user});
                }
                
            }
            
        }); 
    })
});
// Save new password using put request
router.put('/savepassword', (req, res) => {
     User.findOne({username: req.body.username}).select('username name email password contact resettoken').exec(function(err, user){
        if(err) throw err;
        if(req.body.password == null || req.body.password == ''){
            res.json({success:false, msg: 'Please Enter new password'});
        }else{
            user.password = req.body.password;

            //Add user with encrypted password
            bcrypt.genSalt(10, (err, salt) => {
                if(err){
                    throw err;
                }else{
                    bcrypt.hash(user.password, salt, (err, hash) => {
                        if(err){
                            throw err;
                        }else{
                            user.password = hash;
                            user.resettoken = false;
                            user.save(function(err){
                                if(err){
                                    res.json({success:false, msg: err});
                                }else{
                                    var mailOptions = {    
                                        from: 'roshniagrawal.it@gmail.com',
                                        to: user.email,
                                        subject: 'Re: Successfully Reset Password',
                                        text: 'Hello ' + user.name + ',Your password have benn reset Successfully. Now you can login in your account.',
                                        html: 'Hello<strong> ' + user.name + '</strong>,<br><br> Your password have benn reset Successfully. Now you can login in your account.'
                                    }

                                    client.sendMail(mailOptions, function(err, response){
                                        if (err){
                                            console.log(err);
                                        }
                                        else {
                                            console.log('Message sent: ' + response.message);
                                        }
                                        client.close();
                                    });
                                    res.json({success:true, msg: 'Successfully changed password '});
                                }
                            })
                        }
                    });
                }
            });
        }
        
     })
})

// Create middelware to get Token for user Profile Authentication and use next on (req, res, next) because it is a middelware
router.use(function(req, res, next){
    var token = req.body.token || req.body.query || req.headers.authorization;
    if(token){
        //verify token
        jwt.verify(token, config.secret , function(err, decoded) {
            if(err){
                res.json({success: false, msg: 'Token Invalid'});
            }else{
                req.decoded = decoded;
                next();
            }
            
        });
    }else{
        res.json({success: false, msg: 'Token not Provided'});
    }
})


// Current User Profile on url: http://localhost/userapi/profile on "get" request
router.post('/userprofile', (req, res) => {
    res.send(req.decoded._doc);
})


module.exports = router;