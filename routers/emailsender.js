const express = require('express');
const router = express.Router();
const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');

router.post('/contact', function(req, res, next){
    var name = req.body.inputname;
    var email = req.body.inputemail;
    var comment = req.body.inputcomment;

    console.log('\nCONTACT FORM DATA: '+ name + ' '+email + ' '+ comment+'\n');
    var options = {
        service : 'SendGrid',
        auth: {
            api_user: 'roshni090',
            api_key: 'asdfghjkl12'
        }
    }
    var client = nodemailer.createTransport(sgTransport(options));

     var mailOptions = {    
        from: email,
        to: 'roshniagrawal.it@gmail.com',
        subject: 'Contact Form Mail',
        text: comment,
        html: 'Name:' + name + '<br/>Email: '+ email + '<br/>Message: '+ comment
    };

    client.sendMail(mailOptions, function(err, response){
        if (err){
            console.log(err);
            res.json({success:false, msg: err});
        }
        else {
            console.log('Message sent: ' + response.message);
            res.json({success:true, msg:'Message sent Successfully'});
        }
        client.close();
    });
});

module.exports = router;