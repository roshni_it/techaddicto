var myApp = angular.module('myApp', ['ngRoute', 'ngSanitize','userController', 'wordpressController', 'contactController', 'emailController', 'userService', 'contactService', 'mainController', 'authServices', 'wordpressService'])

.config(function($httpProvider){
    $httpProvider.interceptors.push('AuthInterceptor'); 
});
