var myApp = angular.module('myApp');

myApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
     $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider.when('/', {
        templateUrl: './views/pages/home.html',
    })
    .when('/about', {
        templateUrl: './views/pages/about.html',
    })
    .when('/register', {
        templateUrl: './views/pages/register.html',
        controller: 'regCtrl',
        controllerAs: 'register',
        authenticated: false
    })
    .when('/login', {
        templateUrl: './views/pages/login.html',
        authenticated: false 
    })
    .when('/logout', {
        templateUrl: './views/pages/logout.html',
        authenticated: true 
    })
    .when('/profile', {
        templateUrl: './views/pages/profile.html',
        authenticated: true 
    })
    .when('/posts', {
        templateUrl: './views/pages/posts.html',
        controller: 'wordpressCtrl',
        controllerAs: 'wordpress',
        authenticated: false 
    })
    .when('/posts/:post_slug', {
        templateUrl: './views/pages/post.html',
        controller: 'wordpressCtrl',
        controllerAs: 'wordpress',
        authenticated: false 
    })
    .when('/contact-us', {
        templateUrl: './views/pages/contact_form.html',
        controller: 'contactCtrl',
        controllerAs: 'contact',
        authenticated: false 
    })
    .when('/activate/:token', {
        templateUrl: './views/pages/activation/activate.html',
        controller: 'activateCtrl',
        controllerAs: 'activate',
        authenticated: false 
    })
    .when('/resend', {
        templateUrl: './views/pages/activation/resend.html',
        controller: 'resendCtrl',
        controllerAs: 'resend',
        authenticated: false 
    })
    .when('/resetusername', {
        templateUrl: './views/pages/reset/username.html',
        controller: 'usernameCtrl',
        controllerAs: 'userctrl',
        authenticated: false 
    })
   .when('/resetpassword', {
        templateUrl: './views/pages/reset/password.html',
        controller: 'passwordCtrl',
        controllerAs: 'passctrl',
        authenticated: false 
    })
    .when('/passwordreset/:token', {
        templateUrl: './views/pages/reset/passwordreset.html',
        controller: 'resetCtrl',
        controllerAs: 'reset',
        authenticated: false 
    })
    .otherwise({
        redirectTo: '/'
    });
  
    
}]);

// Prevent Url that are not Autherised
myApp.run(['$rootScope', 'Auth', '$location' , function($rootScope, Auth, $location){
    $rootScope.$on('$routeChangeStart', function(event, next, current){
        if(next.$$route.authenticated == true){
           if(!Auth.isLoggedIn()){
               event.preventDefault();
               $location.path('/');
           }
        }else if(next.$$route.authenticated == false){
            if(Auth.isLoggedIn()){
               event.preventDefault();
               $location.path('/profile');
           }
        } 
    });
}]);


