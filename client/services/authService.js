angular.module('authServices', [])

.factory('Auth', function(AuthToken, $http){
    var authFactory = {};
    //Auth.login(loginData);
    authFactory.login = function(loginData){
        return $http.post('/userapi/authenticate', loginData).then(function(data){
            AuthToken.setToken(data.data.token);
            return data;
        });
    };

    // User Profile Function
    //Auth.isLoggedIn();
    authFactory.isLoggedIn = function(){
        if(AuthToken.getToken()){
            return true;
        }else{
            return false;
        }
    };

    // get UserProfile
   /* Auth.getUser();*/
    authFactory.getUser = function(){
        if(AuthToken.getToken()){
            return $http.post('/userapi/userprofile');
        }else{
            $q.reject({success:false, msg: 'User has no token'});
        }
    };

    // LogOut Function
    //Auth.logout();
    authFactory.logout = function(){
        AuthToken.setToken();
    }

    return authFactory;
})

.factory('AuthToken', function($window){
    var authTokenFactory = {
        //AuthToken.setToken(token);
        setToken: function(token){
            if(token){
                $window.localStorage && $window.localStorage.setItem('token', token);
            }else{
                $window.localStorage && $window.localStorage.removeItem('token');
            }
            
            return this;
        },
        //AuthToken.getToken();
        getToken: function(){
            return $window.localStorage && $window.localStorage.getItem('token');
        }
    };


    return authTokenFactory;
})

// Add Factory to get token for each and every request
.factory('AuthInterceptor', function(AuthToken){
    var AuthInterceptorFactory = {};
    AuthInterceptorFactory.request =function(config){
        var token = AuthToken.getToken();
        if(token){
            config.headers.authorization = token;
        }
        return config;
    }
    return AuthInterceptorFactory;
})