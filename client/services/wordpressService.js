angular.module('wordpressService', [])

.factory('Wordpress', function($http,$q){
    const wordpressFactory = {};
    const url = 'http://localhost/techaddicto/wp-json/wp/v2/';

    //Wordpress.getPosts();
    wordpressFactory.getPosts = function(){
        return $http.get(url+'posts').then(handleSuccess, handleError);
    }

    wordpressFactory.getMediaDataForId = function(mediaId){
       
        return $http.get(url+'media/'+ mediaId).then(handleSuccess, handleError);
       //  console.log(url+'media/'+ mediaId);
    }

    function handleSuccess(res){
        return res.data;
    }
    function handleError(res){
        if (!angular.isObject(res.data) || !res.data.message) {
        return($q.reject("An unknown error occurred."));
        }
        return($q.reject(response.data.message));
    }

    return wordpressFactory;
});