angular.module('userService',[])

.factory('User', function($http){
    var userFactory = {};
    //User.create(regData);
    userFactory.create = function(regData){
        return $http.post('/userapi/register', regData);
    }

    //User.checkusername(regData);
    userFactory.checkusername = function(regData){
        return $http.post('/userapi/checkusername', regData);
    }

    //User.checkemail(regData);
    userFactory.checkemail = function(regData){
        return $http.post('/userapi/checkemail', regData);
    }

    //User.activateAccount(token);
    userFactory.activateAccount = function(token){
        return $http.put('/userapi/activate/'+ token);
    }

    //User.checkCredential(loginData);
    userFactory.checkCredential = function(loginData){
        return $http.post('/userapi/resend', loginData);
    }

    //User.resendActivationLink(username);
    userFactory.resendActivationLink = function(username){
        return $http.put('/userapi/resend', username);
    }

    //User.resetUsername(userData);
    userFactory.resetUsername = function(userData){
         return $http.get('/userapi/resetusername/'+userData );
    }

    //User.sendPassword(resetData);
    userFactory.sendPassword = function(resetData){
         return $http.put('/userapi/resetpassword/', resetData);
    }

     //User.resetPassword(token);
    userFactory.resetPassword = function(token){
         return $http.get('/userapi/resetpassword/'+ token);
    }

    //User.savePassword(regData);
    userFactory.savePassword = function(regData){
         return $http.put('/userapi/savepassword', regData);
    }


    return userFactory;
})


