angular.module('contactService', [])

.factory('ContactForm', function($http){
  var contactFormFactory = {};

   //ContactForm.sendEmail(regData);
    contactFormFactory.sendEmail = function(regData){
        return $http.post('/emailapi/contact', regData);
    }
      
   return contactFormFactory;
   
});