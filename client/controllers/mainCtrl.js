angular.module('mainController', ['authServices'])

.controller('mainCtrl', function(Auth, $timeout, $location, AuthToken, $rootScope, $interval, $window ){
    var app = this;
    app.loadme = false;

    //CheckSession function to check if session has expired or not
    app.checkSession = function(){
        if(Auth.isLoggedIn()){
            app.checkingSession = true;
            var interval = $interval(function(){
                var token = $window.localStorage.getItem('token');
                if(token === null){
                    $interval.cancel(interval);
                }else{
                    self.parseJwt = function(token) {
                        var base64Url = token.split('.')[1];
                        var base64 = base64Url.replace('-', '+').replace('_', '/');
                        return JSON.parse($window.atob(base64));
                    }
                    var expireTime = self.parseJwt(token);
                    var currentTime = Math.floor(Date.now()/1000);
                    console.log(expireTime.exp);
                    console.log(currentTime);
                    var remainingTime = (expireTime.exp - currentTime);
                    console.log('Remain Time:' + remainingTime);
                    if(remainingTime <= 0){
                        console.log('token has expired');
                        $interval.cancel(interval);
                        showModel();
                    }else{
                       console.log('token has not expired yet');
                    }
                }



            }, 3000);
        }
    }
    app.checkSession();

    // Open MOdel to Check user session is expiring
    var showModel = function(){
        app.modalHeader= 'Session Timeout Warring!';
        app.modalBody = 'Your session will be expires in 5 min. Would you like to renew session?';
        $('#myModal').modal({backdrop: 'static'})
    }

    app.renewSession = function(){
        console.log('session has been renewed');
    }
    app.endSession = function(){
        console.log('session has Ended');
    };


    // register listener to watch route changes (When we Logout or login etc)
    $rootScope.$on('$routeChangeStart', function(event, next, current){
        if(!app.checkingSession) app.checkSession();
        if(Auth.isLoggedIn()){
            app.isLoggedIn = true;
            Auth.getUser().then(function(data){
            app.username = data.data.username;
            app.name = data.data.name;
            app.email = data.data.email;
            app.loadme = true;
            })
        }else{
            app.isLoggedIn = false;
            app.username = '';
            app.loadme = true;
            
        }
    })

    app.loginUser = function(loginData){
        app.loading = true;
        app.errorMsg = false;
        app.expired = false;
        Auth.login(app.loginData).then(function(data){
            if(data.data.success){
                app.loading = false;
                app.successMsg = data.data.msg + '......Redirecting';
                $timeout(function(){
                    $location.path('/profile');
                    app.loginData = '';
                    app.successMsg = false;
                    app.checkSession();
                }, 3000);
                
            }else{
                if(data.data.expired){
                    app.expired = true;
                    app.loading = false;
                    app.errorMsg = data.data.msg;
                    $timeout(function(){
                        app.errorMsg = false;
                    }, 3000);
                }else{
                    app.loading = false;
                    app.errorMsg = data.data.msg;
                    $timeout(function(){
                        app.errorMsg = false;
                    }, 3000);
                }
               
            }
        })
     }

     // LogOut Function
     app.logout = function(){
         Auth.logout();
         $location.path('/logout');
         $timeout(function(){
            $location.path('/');  
         }, 3000)
     }
})