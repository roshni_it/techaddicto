angular.module('userController',['userService'])

.controller('regCtrl', function($http, $timeout, $location, User){
    var app = this;
     app.regUser = function(regData, valid){
        app.loading = true;
        app.errorMsg = false;
        if(valid){
            User.create(app.regData).then(function(data){
                if(data.data.success){
                    app.loading = false;
                    app.successMsg = data.data.msg + '......Redirecting';
                    $timeout(function(){
                        $location.path('/login');
                    }, 3000);
                }else{
                    app.loading = false;
                    app.errorMsg = data.data.msg;
                    $timeout(function(){
                        app.errorMsg = false;
                    }, 3000);
                }
            });
        }else{
            app.loading = false;
            app.errorMsg = "Please Ensure that Form is filled out properly.";
        }
    };

    // checkUsername(regData);
    app.checkUsername = function(regData){
        app.checkingUsername = true;
        app.usernameMsg = false;
        app.usernameInvalid = false;
        User.checkusername(app.regData).then(function(data){
            if(data.data.success){
                app.checkingUsername = false;
                app.usernameInvalid = false;
                app.usernameMsg = data.data.msg;
            }else{
                app.checkingUsername = false;
                app.usernameInvalid = true;
                app.usernameMsg = data.data.msg;
            }
        });
    };

    // checkEmail(regData);
    app.checkEmail = function(regData){ 
        app.checkingEmail = true;
        app.emailMsg = false;
        app.emailInvalid = false;
        User.checkemail(app.regData).then(function(data){
            if(data.data.success){
                app.checkingEmail = false;
                app.emailInvalid = false;
                app.emailMsg = data.data.msg;
            }else{
                app.checkingEmail = false;
                app.emailInvalid = true;
                app.emailMsg = data.data.msg;
            }
        });
    };
})

.directive('match', function(){
    return {
        restrict: 'A',
        controller: function($scope){
            $scope.doConfirm = function(value){
                value.forEach(function(element) {
                    $scope.confirmed = false;
                    if($scope.confirm == element){
                      $scope.confirmed = true;  
                    }else{
                        $scope.confirmed = false;
                    }
                });
            }
        },
        link: function(scope, element, attrs){
            attrs.$observe('match', function(){
                scope.matches = JSON.parse(attrs.match);
                scope.doConfirm(scope.matches);
            });
             scope.$watch('confirm', function(){
                scope.matches = JSON.parse(attrs.match);
                scope.doConfirm(scope.matches);
            })
        }
    };
})


