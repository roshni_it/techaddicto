angular.module('contactController', ['contactService'])

.controller('contactCtrl', function($http, ContactForm, $timeout, $location ){
    var app = this;
    app.sendContact = function(regData){
        app.loading = true;
        app.errorMsg = false;
        ContactForm.sendEmail(app.regData).then(function(data){
            if(data.data.success == true){
                app.loading = false;
                app.successMsg = data.data.msg + '......Redirecting';
                $timeout(function(){
                    $location.path('/');
                    app.regData = '';
                    app.successMsg = false;
                }, 5000);
            }else{
                app.loading = false;
                app.errorMsg = data.data.msg;
                $timeout(function(){
                    app.errorMsg = false;
                }, 3000);
            }
        });
    }
   
});