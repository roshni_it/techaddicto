angular.module('emailController', ['userService'])

.controller('activateCtrl', function($routeParams, User, $timeout, $location){
    var app = this;
    User.activateAccount($routeParams.token).then(function(data){
         app.successMsg = false;
         app.errorMsg = false;
         if(data.data.success){
            app.successMsg = data.data.msg;
            $timeout(function(){
                $location.path('/login');
            }, 3000);
         }else{
            app.errorMsg = data.data.msg;
            $timeout(function(){
                app.errorMsg = false;
                $location.path('/login');
            }, 3000);
         }
    })
}) 

.controller('resendCtrl', function($routeParams, User, $timeout, $location){
    var app = this;
    app.checkCredentials = function(loginData){
        User.checkCredential(app.loginData).then(function(data){
           // console.log(data);
            app.errorMsg = false;
            app.successMsg = false;
            if(data.data.success){
                 User.resendActivationLink(app.loginData).then(function(data){
                    if(data.data.success){
                        app.successMsg = data.data.msg;
                        $timeout(function(){
                            app.successMsg = false;
                            app.loginData = '';  
                            $location.path('/login');
                        }, 3000);
                    }else{
                        app.errorMsg = data.data.msg;
                    }
                  });
            }else{
                app.errorMsg = data.data.msg;
             }
        })
    }
}) 

.controller('usernameCtrl', function(User, $timeout, $location){
    var app = this;
    app.sendUsername = function(userData){
        User.resetUsername(app.userData.email).then(function(data){
            app.errorMsg = false;
            app.successMsg = false;
            if(data.data.success){
                app.successMsg = data.data.msg;
                $timeout(function(){
                    app.successMsg = false;
                    app.loginData = '';  
                    $location.path('/login');
                }, 3000);
            }else{
                 app.errorMsg = data.data.msg;
            }
        })
    }
})


.controller('passwordCtrl', function(User, $timeout, $location){
    var app = this;

   app.sendPassword = function(resetData){
        User.sendPassword(app.resetData).then(function(data){
            app.errorMsg = false;
            app.successMsg = false;
            if(data.data.success){
                app.successMsg = data.data.msg;
                $timeout(function(){
                    app.successMsg = false;
                    app.loginData = '';  
                    $location.path('/login');
                }, 3000);
            }else{
                 app.errorMsg = data.data.msg;
            }
        })
    }
})

.controller('resetCtrl', function(User, $timeout, $location, $routeParams, $scope){
    var app = this;
    app.hide = true;
   User.resetPassword($routeParams.token).then(function(data){
       app.errorMsg = false;
        app.successMsg = false;
       if(data.data.success){
           app.hide = false;
           app.successMsg = 'Please Enter a new password';
           $scope.username = data.data.user.username;
       }else{
           app.hide = true;
           app.errorMsg = data.data.msg;
       }
   })

   app.savePassword = function(regData, valid, confirmed){
        app.errorMsg = false;
        app.successMsg = false;
       if(valid && confirmed){
            app.regData.username = $scope.username;
            User.savePassword(app.regData).then(function(data){
                if(data.data.success){
                    app.successMsg = data.data.msg;
                    $timeout(function(){
                        $location.path('/login')
                    }, 3000);
                }else{
                    app.successMsg = false;
                    app.errorMsg = data.data.msg;
                }
            })
       }else{
           app.errorMsg = "Please Enter new password and Confirmed password before submit.";
       }
   }
})