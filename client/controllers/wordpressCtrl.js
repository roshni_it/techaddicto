angular.module('wordpressController', ['wordpressService'])

.controller('wordpressCtrl', function(Wordpress){
    var app = this;

    app.posts = [];
    app.images = {};
    Wordpress.getPosts().then(function (data) {
        app.posts = data;
        angular.forEach(data, function(value, index) {
        app.setUrlForImage(index, value.featured_media);
        });
    }, function error(err) {
        console.log('Errror: ', err);
    });

    app.setUrlForImage = function(index, mediaId){
        Wordpress.getMediaDataForId(mediaId).then(function(success){
            app.images[index] = success.source_url;
        });
    }


});
